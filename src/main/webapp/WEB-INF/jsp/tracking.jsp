<%@ taglib prefix="spring" uri="http://www.springframework.org/tags" %> 
<%@ page language="java" contentType="text/html; charset=ISO-8859-1"
    pageEncoding="ISO-8859-1"%>   
<%@ taglib prefix="form" uri="http://www.springframework.org/tags/form" %>



<!DOCTYPE html PUBLIC "-//W3C//DTD HTML 4.01 Transitional//EN" "http://www.w3.org/TR/html4/loose.dtd">
<html>
<head>
<meta http-equiv="Content-Type" content="text/html; charset=ISO-8859-1">

<script type="text/javascript" src="jquery-1.8.3.js"></script>

<!-- <script type="text/javascript">
	$(document).ready(
		function() {
			$.getJSON('<spring:url value="emptyLockers.json"/>', {
				ajax : 'true'
			}, function(data){
				var html = '<option value="">--Please select one--</option>';
				var len = data.length;
				for (var i = 0; i < len; i++) {
					html += '<option value="' + data[i].id + '">'
							+ data[i].id + '</option>';
				}
				html += '</option>';
				
				$('#emptyLockers').html(html);
			});			
		});
	
</script> -->

<title>LockerTracker</title>
</head>
<body>

<h1></h1>

<h1>User: ${employee.name}</h1>
<h2>Locker id: ${employee.lockerId}</h2>

<br>
<h3>Empty lockers:</h3>
${emptyLockers}

<br>
<h3>Reserved lockers:</h3>
${reservedLockers}

<br>
Enter the locker id
<form:form commandName="employee">
	<form:select path="lockerId" items="${emptyLockers}" itemValue="id" itemLabel="id"></form:select>
	<input type="submit" value="Enter"/>
</form:form>

</body>
</html>