package com.epam.service;

import java.io.BufferedReader;
import java.io.File;
import java.io.FileNotFoundException;
import java.io.FileReader;
import java.io.IOException;
import java.util.ArrayList;
import java.util.List;

import org.springframework.stereotype.Service;

import com.epam.model.Locker;

@Service("lockerService")
public class LockerService {

    public List<Locker> listEmptyLockers() throws FileNotFoundException, IOException{


        List<Locker> emptyLockers = new ArrayList<>();

        String[] lineSplit = null;
        File fileEmptyLocker = new File("C:\\Java Spring WorkSpace\\LockerTracker\\resources\\emptyLockers.txt");


        StringBuilder builder = new StringBuilder();
        BufferedReader reader = null;
        try {
            reader = new BufferedReader(new FileReader(fileEmptyLocker.getAbsoluteFile()));
            String line = null;
            int index=0;
            while ((line = reader.readLine()) != null) {
                builder.append(line);
                lineSplit=line.split(":");
                int id = Integer.parseInt(lineSplit[0]);
                emptyLockers.add(new Locker());
                emptyLockers.get(index).setId(id);
                emptyLockers.get(index).setEmployeeName(lineSplit[1]);
                index++;
            }
        } catch (IOException e) {
            System.err.println("BiszemBaszom");
        }

        System.out.println();
        System.out.println("Empty lockers:");
        for ( Locker locker : emptyLockers) {
            System.out.println(locker.getId() + " " + locker.getEmployeeName());
        }

        return emptyLockers;
    }


    public List<Locker> listReservedLockers() throws FileNotFoundException, IOException{


        List<Locker> reservedLockers = new ArrayList<>();

        String[] lineSplit = null;
        File fileReservedLocker = new File("C:\\Java Spring WorkSpace\\LockerTracker\\resources\\reservedLockers.txt");


        StringBuilder builder = new StringBuilder();
        BufferedReader reader = null;
        try {
            reader = new BufferedReader(new FileReader(fileReservedLocker.getAbsoluteFile()));
            String line = null;
            int index=0;
            while ((line = reader.readLine()) != null) {
                builder.append(line);
                lineSplit=line.split(":");
                int id = Integer.parseInt(lineSplit[0]);
                reservedLockers.add(new Locker());
                reservedLockers.get(index).setId(id);
                reservedLockers.get(index).setEmployeeName(lineSplit[1]);
                index++;
            }
        } catch (IOException e) {
            System.err.println("BiszemBaszom");
        }

        System.out.println();
        System.out.println("Reserved lockers:");
        for ( Locker locker : reservedLockers) {
            System.out.println(locker.getId() + " " + locker.getEmployeeName());
        }

        return reservedLockers;
    }


}
