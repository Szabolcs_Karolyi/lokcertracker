package com.epam.controller;

import java.io.FileNotFoundException;
import java.io.IOException;
import java.util.HashMap;
import java.util.List;
import java.util.Map;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Controller;
import org.springframework.web.bind.annotation.ModelAttribute;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestMethod;
import org.springframework.web.bind.annotation.ResponseBody;
import org.springframework.web.bind.annotation.SessionAttributes;
import org.springframework.web.servlet.ModelAndView;

import com.epam.model.Employee;
import com.epam.model.Locker;
import com.epam.service.LockerService;

@Controller
@SessionAttributes({ "employee", "emptyLockers" })
public class TrackerController {

    @Autowired
    private LockerService lockerService;

    @RequestMapping(value = "/tracking", method = RequestMethod.GET)
    public ModelAndView tracking(@ModelAttribute("employee") Employee employee, ModelAndView model) {

        System.out.println("Employee's name : " + employee.getName());
        System.out.println("Employee's id : " + employee.getLockerId());
        List<Locker> emptyLockers = null;
        List<Locker> reservedLockers = null;
        Map<String, List<Locker>> lockerMap = new HashMap<>();

        try {
        emptyLockers = lockerService.listEmptyLockers();
        reservedLockers = lockerService.listReservedLockers();
        } catch (Exception e) {
            e.printStackTrace();
        }
        if(employee.getLockerId()!=0){
            Locker kurvaaa = null;
            for(Locker locker : emptyLockers) {
                if(locker.getId()==employee.getLockerId()) {
                    kurvaaa = locker;
                    Locker locker2 = new Locker();
                    locker2.setEmployeeName(employee.getName());
                    locker2.setId(employee.getLockerId());
                    reservedLockers.add(locker2);

                }
            }
            emptyLockers.remove(kurvaaa);
        }
        lockerMap.put("emptyLockers", emptyLockers);
        lockerMap.put("reservedLockers", reservedLockers);
        return new ModelAndView("tracking", lockerMap);
    }

    @RequestMapping(value = "/emptyLockers", method = RequestMethod.POST)
    public @ResponseBody List<Locker> listAllLocker() throws FileNotFoundException, IOException {
        return lockerService.listEmptyLockers();
    }

    @RequestMapping(value = "/tracking", method = RequestMethod.POST)
    public ModelAndView trackingpost(@ModelAttribute("employee") Employee employee) {
        System.out.println("tracking POST");

        return new ModelAndView("redirect:tracking.html", "employee", employee);
    }

}
