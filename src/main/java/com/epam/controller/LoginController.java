package com.epam.controller;

import org.springframework.stereotype.Controller;
import org.springframework.validation.BindingResult;
import org.springframework.web.bind.annotation.ModelAttribute;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestMethod;
import org.springframework.web.bind.annotation.SessionAttributes;
import org.springframework.web.servlet.ModelAndView;

import com.epam.model.Employee;

@Controller
@SessionAttributes("employee")
public class LoginController {

    @RequestMapping(value = "login", method = RequestMethod.GET)
    public ModelAndView login() {
        Employee employee = new Employee();

        return new ModelAndView("login", "employee", employee);
    }

    @RequestMapping(value = "login", method = RequestMethod.POST)
    public ModelAndView updateEmployee(@ModelAttribute("employee") Employee employee, BindingResult result) {

        System.out.println("result has errors: " + result.hasErrors());

        System.out.println("User name: " + employee.getName());

        if (result.hasErrors()) {
            return new ModelAndView("login");
        }

        return new ModelAndView("redirect:tracking.html", "employee", employee);
    }
}
