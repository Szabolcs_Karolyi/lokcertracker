package com.epam.model;

public class Employee {
    private String name;
    private int lockerId;

    public String getName() {
        return name;
    }
    public void setName(String name) {
        this.name = name;
    }
    public int getLockerId() {
        return lockerId;
    }
    @Override
    public String toString() {
        return "Employee [name=" + name + "]";
    }
    public void setLockerId(int lockerId) {
        this.lockerId = lockerId;
    }
}
